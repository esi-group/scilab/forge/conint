Conint toolbox

Purpose
----

The goal of this toolbox is to provide functions to 
compute confidence intervals for statistical estimators.
These confidence intervals are computed based on various 
probability distribution functions.

The toolbox is based on macros.

Features
--------

The following is a list of the current accsum functions :
 * conint_bernoullip � C.I. of the probability of a Bernoulli variable.
 * conint_bernoullipnumber � Number of experiments for a Bernoulli variable.
 * conint_normmu � C.I. of the mean of a normal variable.
 * conint_normmunumber � Number of experiments for a Normal variable.
 * conint_normvar � C.I. of the variance of a normal variable.

Dependencies
----

 * This modules depends on the assert module.
 * This modules depends on the helptbx module.
 * This modules depends on the apifun module.

Author
------

Copyright (C) 2011 - Michael Baudin

Forge
-----

http://forge.scilab.org/index.php/p/conint/

ATOMS
-----

Not yet

Licence
----

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Bibliography
------------
 * http://en.wikipedia.org/wiki/Confidence_interval
 * "Introduction to probability and statistics for engineers and scientists", 
   Sheldon Ross, Third Edition, 2004
 * "Probabilit�s, Analyse de Donn�es et Statistique", Gilbert Saporta, 2nd Ed., 2006
 * "Probability and Statistics for Engineers and Scientists", 
   Ronald Walpole, Raymond Myers, Sharon Myers, Keying Ye, Eight Edition, 2006
