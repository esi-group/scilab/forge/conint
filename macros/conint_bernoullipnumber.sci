// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function n = conint_bernoullipnumber ( varargin )
    // Number of experiments for a Bernoulli variable.
    //
    // Calling Sequence
    //   n = conint_bernoullipnumber ( len )
    //   n = conint_bernoullipnumber ( len , level )
    //   n = conint_bernoullipnumber ( len , level , pe )
    //
    // Parameters
    //   len : a 1-by-1 matrix of doubles, positive, the length of the required C.I.
    //   level : a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range [0.,0.5]
    //   pe : a 1-by-1 matrix of doubles, an estimate of the probability of success (default pe = 0.5)
    //   n : a 1-by-1 matrix of doubles, the number of trials
    //
    // Description
    // Returns the number of experiments which guarantees that the 
    // length of the confidence interval for the 
    // probability of success of a Bernoulli variable 
    // is less than <literal>len</literal>. 
    // 
    // In other words, if <literal>pe</literal> is the estimate of 
    // the probability of success 
    // with <literal>n</literal> trials, then 
    //
    // <screen>
    // [low, up] = conint_bernoullip ( pe , n )
    // </screen>
    //
    // is so that 
    //
    // <literal>
    // abs(up-low) <= len
    // </literal>
    //
    // To compute <literal>n</literal>, we assume that the random variable 
    // has Binomial distribution.
    //
    // This function makes the assumption that the data 
    // in <literal>x</literal> has a binomial distribution with 
    // parameters <literal>p</literal> and 
    // <literal>n</literal>.
    //
    // The default value <literal>pe=0.5</literal> 
    // guarantees that the number of experiments is large enough, 
    // no matter the actual value of the probability <literal>p</literal>.
    // In other words, if <literal>pe</literal> is not provided 
    // be the user, the value of the output variable <literal>n</literal> 
    // is guaranteed, but might be much too large than required.
    //
    // Examples
    // // From Gilbert Saporta, 
    // // Section 13.5.4 Intervalle de confiance 
    // // pour une proportion p
    // // p.313
    // len = 0.05*2
    // level = 1.-0.90
    // n = conint_bernoullipnumber ( len , level )
    // // Therefore, it requires 271 experiments 
    // // to make sure that the 90% C.I. for p 
    // // has a length no greater than 0.1.
    // 
    // // Reproduce the table p 313.
    // mprintf("level = %9s     %9s %9s %9s", ..
    //  " ", "1.-0.90", "1.-0.95", "1.-0.98")
    // for len = 2*[0.01 0.02 0.05]
    //     mprintf("len = %9s, n = ", string(len))
    //     for level = 1.-[0.90 0.95 0.98]
    //         n = conint_bernoullipnumber ( len , level );
    //         mprintf("%9s ", string(n));
    //     end
    //     mprintf("\n");
    // end
    // 
    // // From Sheldon Ross,
    // // Example 7.5c
    // // Estimate the failure rate of computer chips.
    // len = 0.05
    // // Compute n for 95% C.I.
    // n = conint_bernoullipnumber ( len )
    // // Compute n for 99% C.I.
    // level = 1.-0.99
    // n = conint_bernoullipnumber ( len , level )
    // // Compute n for 99% C.I. and probability estimate
    // // From 30 sample computer chips, there are 
    // // 26. chips which works.
    // pe = 26. / 30.
    // n = conint_bernoullipnumber ( len , level , pe )
    // // It may require 1227 experiments to get a 
    // // 99 C.I. with length less than 0.05.
    //
    // // How many experiments to estimate accurately a 
    // // small probability with 95% C.I. ?
    // level = 1.-0.95;
    // for pe = logspace(-1,-7,7)
    //     len = pe/10.;
    //     n = conint_bernoullipnumber ( len , level , pe );
    //     mprintf("pe = %-9s, len = %-9s, n = %-9s\n", ..
    //     string(pe), string(len), string(n))
    // end
    // 
    // Authors
    //   Copyright (C) 2011 - Michael Baudin
    //
    // Bibliography
    //    http://en.wikipedia.org/wiki/Confidence_interval
    //    "Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004
    //    "Probabilités, Analyse de Données et Statistique", Gilbert Saporta, 2nd Ed., 2006

    [lhs, rhs] = argn();
    apifun_checkrhs ( "conint_bernoullipnumber" , rhs , 1 : 3 )
    apifun_checklhs ( "conint_bernoullipnumber" , lhs , 0 : 3 )
    //
    // Get arguments
    len = varargin(1)
    level = apifun_argindefault ( varargin , 2 , 1.-0.95 )
    pe = apifun_argindefault ( varargin , 3 , 0.5 )
    //
    // Check type
    apifun_checktype ( "conint_bernoullipnumber" , len ,   "len" ,  1 , "constant" )
    apifun_checktype ( "conint_bernoullipnumber" , level, "level" , 2 , "constant" )
    apifun_checktype ( "conint_bernoullipnumber" , pe, "pe" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "conint_bernoullipnumber" , len ,   "len" ,   1 )
    apifun_checkscalar ( "conint_bernoullipnumber" , level , "level" , 2 )
    apifun_checkscalar ( "conint_bernoullipnumber" , pe ,    "pe" ,    3 )
    //
    // Check content
    apifun_checkgreq ( "conint_bernoullipnumber" , len , "len" , 1 , 0. )
    apifun_checkrange ( "conint_bernoullipnumber" , level , "level" , 2 , 0. , 0.5 )
    apifun_checkrange ( "conint_bernoullipnumber" , pe , "pe" , 1 , 0. , 1. )
    //
    q = level/2.
    p = 1.-q
    f = cdfnor("X",0.,1.,p,q)
    n = (2*f)^2*pe*(1.-pe) / (len^2)
    n = ceil(n)
endfunction
